import itunespy
from flask import Flask, render_template, request


def get_matches(object_container, target_class):
    return [obj for obj in object_container if isinstance(obj, target_class)]


app = Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def index():
    if request.method == "POST":
        text = request.form["search"]

        tracks = get_matches(itunespy.search_track(text), itunespy.track.Track)
        music_artists = get_matches(itunespy.search_artist(text), itunespy.music_artist.MusicArtist)
        music_albums = get_matches(itunespy.search_album(text), itunespy.music_album.MusicAlbum)

        print("TRACKS: ", tracks)
        print("ALBUMS: ", music_albums)
        print("MUSIC ARTISTS: ", music_artists)

        return render_template(
            "views/index.html",
            term=text,
            tracks=tracks,
            music_artists=music_artists,
            music_albums=music_albums
        )

    return render_template("views/index.html")


if __name__ == "__main__":
    app.run(debug=True)